**Molette de Scroll DIY**

Cette molette a été créée par Mr Cristina, adhérent du fablab des Fabriques, pour un de ses adhérents porteur d'un handicap l'empêchant d'utiliser facilement ses mains. Il pilote son ordinateur avec un gros trackball qu'il pilote avec son pied, mais il lui manquait une molette de scroll. Un arduino Leonardo peut se faire passer pour une souris, et un encodeur est utilisé pour capter le sens de rotation. Une série d'aimants est utilisé pour cranter artificiellement la roue.

Encodeur utilisé: https://fr.farnell.com/broadcom-limited/aeat-6010-a06/codeur-magnetique-10-bits-12000rpm/dp/2467467

![](Pics/Image1668218712.jpg)
![](Pics/Image139694902.jpg)
![](Pics/Image31422971.jpg)