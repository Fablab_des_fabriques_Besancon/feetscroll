// Read in 10-bits Magnetic Encoder AEAT-6010-A06  into Arduino Uno
// RobinL
//From https://forum.arduino.cc/index.php?topic=164353.0

// Declarate

#include <Mouse.h>

#define UP 1
#define DOWN 0

const int CSn = 4; // Chip select
const int CLK = 7; // Clock signal
const int DO = 8; // Digital Output from the encoder which delivers me a 0 or 1, depending on the bar angle..

int sensorWaarde = 0;
int previousSensorWaarde = 0;
int currentValue = 0;
int previousCurrentValue = 0;
int lastCurrentValue = 0;
int delta = 25;
int scroll_direction;
int lastScroll_direction;

int dirCount;

unsigned long interval;
unsigned long last_interval = 0;
int min_interval = 80;

void setup() {
  Serial.begin(115200);

  pinMode(CSn, OUTPUT);
  pinMode(CLK, OUTPUT);
  pinMode(DO, INPUT);

  //Let's start here
  digitalWrite(CLK, HIGH);
  digitalWrite(CSn, HIGH);

  Mouse.begin();
}



void loop() {

  sensorWaarde = readSensor();

  if (abs(sensorWaarde - previousSensorWaarde) > delta)
  {

    //if (abs(currentValue - previousCurrentValue)>delta/2)previousCurrentValue=currentValue;
    previousSensorWaarde = sensorWaarde;
    previousCurrentValue = currentValue;
    currentValue = sensorWaarde;
  }

  if (currentValue != 0 && currentValue != 1023 && previousCurrentValue != 0 && previousCurrentValue != 1023 &&  lastCurrentValue != currentValue) // Glitch when 0 or 1023
  {
    if (scroll_direction == lastScroll_direction)dirCount++;
    // On valide le chgt de direction que si interval-last_interval > min_interval
    if ((currentValue - previousCurrentValue) > 8/* && (currentValue - previousCurrentValue) < 100 || (currentValue < 15) && (previousCurrentValue > 1015)*/)
    {
      scroll_direction = DOWN;
      
      interval = millis();
      if (scroll_direction != lastScroll_direction && interval - last_interval > min_interval)
      {
        //Serial.print(interval - last_interval);
        Serial.println("CHANGE");
        lastScroll_direction = scroll_direction;
        dirCount = 0;

      }
      /*
      Serial.print(currentValue);
      Serial.print(" ");
      Serial.print(currentValue - previousCurrentValue);
      Serial.print(" ");
      */
      //if (interval-last_interval>min_interval)
      //{
      if (scroll_direction == lastScroll_direction || dirCount>3)
      {
        Serial.println("DOWN");
        Mouse.move(0, 0, -1);
      }

      lastCurrentValue = currentValue;
      last_interval = interval;

    } else if ((currentValue - previousCurrentValue) < -8 /*&& (currentValue - previousCurrentValue) > -100 || (currentValue > 1015) && (previousCurrentValue < 15)*/) {
      scroll_direction = UP;
      interval = millis();
      if (scroll_direction != lastScroll_direction && interval - last_interval > min_interval)
      {
        Serial.println("CHANGE");
        lastScroll_direction = scroll_direction;
        dirCount = 0;
      }
      /*
      Serial.print(currentValue);
      Serial.print(" ");
      Serial.print(currentValue - previousCurrentValue);
      Serial.print(" ");
      */
      if (scroll_direction == lastScroll_direction || dirCount>3)
      {
        Serial.println("UP");
        Mouse.move(0, 0, 1);
      }
      lastCurrentValue = currentValue;
      last_interval = interval;
    }


  }

  delayMicroseconds(1); //Tcs waiting for another read in
}

unsigned int readSensor() {
  unsigned int dataOut = 0;

  digitalWrite(CSn, LOW);
  delayMicroseconds(1); //Waiting for Tclkfe

  //Passing 10 times, from 0 to 9
  for (int x = 0; x < 10; x++) {
    digitalWrite(CLK, LOW);
    delayMicroseconds(1); //Tclk/2
    digitalWrite(CLK, HIGH);
    delayMicroseconds(1); //Tdo valid, like Tclk/2
    dataOut = (dataOut << 1) | digitalRead(DO); //shift all the entering data to the left and past the pin state to it. 1e bit is MSB
  }

  digitalWrite(CSn, HIGH); //
  return dataOut;
}
